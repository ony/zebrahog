# -*- coding: utf-8 -*-

import irc3
import traceback
from irc3.plugins.command import command

import zebrahog
from zebrahog import gitlab, patch_list
from zebrahog_import import patch_import


@irc3.plugin
class Plugin:

    def __init__(self, bot):
        self.bot = bot
        self.client = irc3.utils.maybedotted('aiohttp.ClientSession')
        self.bot.loop.set_debug(True)

    @command(aliases=['pl'])
    async def patchlist(self, mask, target: str, args):
        """List pending patches (query by ::repo_name or keyword)

            %%patchlist [<query>]
        """
        print(mask, target, args)
        try:
            async with self.client(loop=self.bot.loop) as session:
                async with gitlab(session) as gl:
                    messages = [message
                                async for message in patch_list(gl, query=args['<query>'])]
                    if len(messages) > 5 and target.startswith('#'):
                        messages[5:] = ["There are more, but I'll tell it in private if you ask me"]
                    return messages
        except Exception as e:
            traceback.print_exc()
            return ['Sorry. ' + str(e)]

    @command(aliases=['pq'])
    async def patchqueue(self, mask, target, args):
        """Import new patch into repo from url

            %%patchqueue <url> <repo> [<comment>...]
        """
        print(mask, target, args)
        comment = ' '.join(args['<comment>']) or None
        try:
            async with self.client(loop=self.bot.loop) as session:
                async with gitlab(session) as gl:
                    return [message
                            async for message in patch_import(gl, patch=args['<url>'], repo_name=args['<repo>'],
                                                              comment=comment)]
        except Exception as e:
            traceback.print_exc()
            return ['Sorry. ' + str(e)]

    @irc3.event(r'(@(?P<tags>\S+) )?:(?P<ns>NickServ)!NickServ@services.'
                r' NOTICE (?P<nick>\S*) :This nickname is registered.*')
    def register(self, ns=None, nick=None, **kw):
        print("NickServ request", ns, nick, kw)
        try:
            password = zebrahog.config[nick + '@nickserv']['password']
        except KeyError:
            pass
        else:
            self.bot.privmsg(ns, 'identify %s %s' % (nick, password))
