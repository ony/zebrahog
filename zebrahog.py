#!/usr/bin/python3
import asyncio
from collections import defaultdict

import aiohttp
import arrow as arrow
import json
import os.path
from aiocache import cached
from async_generator import asynccontextmanager
from diskcache import Cache
from gidgetlab import BadRequest
from gidgetlab.aiohttp import GitLabAPI
from http import HTTPStatus
from poyo import parse_string
from subprocess import CalledProcessError
from typing import Optional, List, Sequence
from urllib.parse import quote as urlquote
from xdg import BaseDirectory


def _key_builder(func, *pargs, **kwargs):
    fqn = func.__name__
    if func.__module__:
        fqn = func.__module__ + '.' + fqn
    # strip instance values
    pargs = [arg.api_url if isinstance(arg, GitLabAPI) else arg
             for arg in pargs]
    return '%s(*%r, **%r)' % (fqn, pargs, kwargs)


CACHE_PATH = BaseDirectory.save_cache_path('zebrahog')
_repo_cache = Cache(os.path.join(CACHE_PATH, 'repo.cache'))

LONG_CACHE = cached(ttl=(30*60), key_builder=_key_builder)


@asynccontextmanager
async def gitlab(session=None):
    if session is None:
        async with aiohttp.ClientSession() as session:
            async with gitlab(session) as gl:
                yield gl
    else:
        gl = GitLabAPI(session,
                       url="https://gitlab.exherbo.org/",
                       requester=credentials['user'],
                       access_token=credentials['token'])
        yield gl


async def fetch_content(gl: GitLabAPI, project_id: int, filepath: List[str]):
    assert len(filepath) > 0
    return await gl.getitem(
        '/projects/{}/repository/files/{}/raw'.format(project_id, urlquote("/".join(filepath), safe='')),
        params={'ref': 'master'})


@LONG_CACHE
async def get_repo_name(gl: GitLabAPI, project_id: int):
    key = b'project:%d' % (project_id,)
    repo_name = _repo_cache.get(key)
    if repo_name is None:
        repo_name = (await fetch_content(gl, project_id, ['profiles', 'repo_name'])).strip()
        _repo_cache.set(key, repo_name.encode('ascii'))
    else:
        repo_name = repo_name.decode('ascii')
    return repo_name


@LONG_CACHE
async def get_project(gl: GitLabAPI, project_id: int):
    """
    Get full details about project
    :param project_id:
    """
    return await gl.getitem('/projects/{}'.format(project_id))


@LONG_CACHE
async def get_user_profile(gl: GitLabAPI):
    return await gl.getitem('/user')


@LONG_CACHE
async def get_user_emails(gl: GitLabAPI) -> List[str]:
    primary = get_user_profile(gl)
    additional_task = gl.getitem('/user/emails')
    return [(await primary)['email']] + [entry['email'] for entry in await additional_task]


async def repos_groups_ids(gl: GitLabAPI):
    # XXX: discover?
    yield 2  # official
    yield 3  # devs
    yield 4  # unofficial


async def find_repo_root_projects(gl: GitLabAPI, repo_name: str):
    key = b'repo:' + repo_name.encode('ascii')
    cached_matches: Optional[bytes] = _repo_cache.get(key)
    if cached_matches is not None:
        for match in json.loads(cached_matches):
            yield match
        return

    found_nothing = True
    repos = defaultdict(lambda: [])
    async for project in gl.getiter('/search',
                                    params={'scope': 'projects', 'search': repo_name}):
        project = await get_project(gl, project['id'])
        if project.get('forked_from_project', None):
            continue
        try:
            candidate_name = await get_repo_name(gl, project['id'])
        except BadRequest as e:
            if e.status_code == HTTPStatus.NOT_FOUND:
                continue
            raise e
        repos[candidate_name].append(project)
        if candidate_name == repo_name:
            found_nothing = False
            yield project
    for name, projects in repos.items():
        key = b'repo:' + name.encode('ascii')
        _repo_cache.set(key, json.dumps(projects))
    if found_nothing:
        raise LookupError("No project ::%s found" % (repo_name,))


async def find_pending_requests(gl: GitLabAPI, query: Optional[str]=None):
    if not query:
        visited = set()
        async for group_id in repos_groups_ids(gl):
            async for request in gl.getiter('/groups/{id}/merge_requests'.format(id=group_id),
                                            params={'state': 'opened'}):
                if request['web_url'] in visited:
                    continue
                visited.add(request['web_url'])
                yield request
    elif query.startswith('::'):
        repo_name = query[2:]
        async for project in find_repo_root_projects(gl, repo_name):
            async for request in gl.getiter('/projects/{id}/merge_requests'.format(**project),
                                            params={'state': 'opened'}):
                request['repo_name'] = repo_name
                yield request
    else:
        async for request in gl.getiter('/search',
                                        params={'scope': 'merge_requests', 'search': query}):
            if request['state'] in ('merged', 'closed'):
                continue
            yield request


async def obtain_fork_project(gl: GitLabAPI, repo_name: str):
    base_projects = []
    forks_fetches = []
    async for project in find_repo_root_projects(gl, repo_name):
        base_projects.append(project)
        forks_fetches.append(gl.getitem('/projects/{id}/forks'.format(**project),
                                        params={'owned': True}))
    fork_projects = [fork
                     for forks_fetch in forks_fetches
                     for fork in await forks_fetch]
    assert len(fork_projects) <= 1, "Ambiguous forks for ::" + repo_name
    if not fork_projects:  # No fork found
        if not base_projects:  # and no base repo found
            raise LookupError("Failed to find repo ::" + repo_name)
        assert len(base_projects) <= 1, 'Ambiguous projects for ::' + repo_name
        [base_project] = base_projects
        try:
            fork_project = await gl.post('/projects/{id}/fork'.format(**base_project), data=b'')
        except BadRequest as e:
            if e.status_code != HTTPStatus.CONFLICT:
                raise
            # try fetching our fork again
            fork_projects = await gl.getitem('/projects/{id}/forks'.format(**base_project), params={'owned': True})
            if not fork_projects:
                raise
            assert len(fork_projects) <= 1, "Ambiguous forks for ::" + repo_name
            return fork_projects[0]
        while True:  # poll for fork to be ready
            state = await gl.getitem('/projects/{id}/import'.format(**fork_project))
            if state.get('import_status', None) == 'finished':
                break
            await asyncio.sleep(5)  # wait a bit
        return fork_project
    else:
        return fork_projects[0]


async def async_check_call(program: str, *args: str, **kwargs):
    proc = await asyncio.create_subprocess_exec(program, *args, **kwargs)
    retcode = await proc.wait()
    if retcode != 0:
        raise CalledProcessError(retcode, (program,) + args)


async def async_check_output(program: str, *args: str, **kwargs) -> str:
    kwargs.setdefault('stdout', asyncio.subprocess.PIPE)
    proc = await asyncio.create_subprocess_exec(program, *args, **kwargs)
    output = (await proc.stdout.read()).decode('utf-8')
    retcode = await proc.wait()
    if retcode != 0:
        raise CalledProcessError(retcode, (program,) + args)
    return output


async def patch_list(gl: GitLabAPI, query: Optional[str]=None, lazy=False):
    async for request in find_pending_requests(gl, query):
        repo_name = request.get('repo_name', None)
        if not repo_name and not lazy:
            try:
                repo_name = await get_repo_name(gl, request['project_id'])
            except BadRequest as e:
                if e.status_code == HTTPStatus.NOT_FOUND:
                    continue
                raise e
        message = request['web_url']
        if repo_name:
            message += ' ::' + repo_name
        message += ' ' + request['title']
        created_at = arrow.get(request['created_at'])
        message += ' (%s)' % (created_at.humanize(),)
        yield message


async def patch_queue(gl: GitLabAPI, repo_name: Optional[str], git_dir: Optional[str]=None,
                      comment: Sequence[str]=tuple(), footer_text: str=None, lazy=False):
    if git_dir:
        base_args = ['git', '--git-dir=' + git_dir]
    else:
        base_args = ['git']

    emails_task = get_user_emails(gl)

    if not repo_name:
        repo_name = (await async_check_output(*base_args, 'show', 'HEAD:profiles/repo_name')).strip()
    elif repo_name.startswith('::'):
        repo_name = repo_name[2:]

    fork_project = await obtain_fork_project(gl, repo_name)
    print('Using fork ' + fork_project['web_url'] + ' for ::' + repo_name)
    branch = 'zebrahog/' + (await async_check_output(*base_args, 'rev-parse', '--abbrev-ref', 'HEAD')).strip()

    author_email = (await async_check_output(*base_args, 'log', '-1', '--format=%aE')).strip()

    if comment:
        summary = ' '.join(comment)
    else:
        summary = (await async_check_output(*base_args, 'log', '-1', '--format=%s')).strip()

    if author_email not in await emails_task:
        author = (await async_check_output(*base_args, 'log', '-1', '--format=%aN')).strip()
        summary = '({}) {}'.format(author, summary)

    description = await async_check_output(*base_args, 'log', 'origin..', '--format=## %s%n%b%n')

    if footer_text:
        description += "---\n" + footer_text

    await async_check_call(*base_args, 'push', fork_project['ssh_url_to_repo'], 'HEAD:' + branch, '--force')

    print('Pushed/created branch ' + branch + ' with ' + summary)
    base_project = fork_project['forked_from_project']
    answer = await gl.post('/projects/{id}/merge_requests'.format(**fork_project),
                           params={'source_branch': branch,
                                   'target_branch': base_project['default_branch'],
                                   'title': summary,
                                   'description': description,
                                   'target_project_id': base_project['id'],
                                   'remove_source_branch': True,
                                   'allow_collaboration': True},
                           data=b'')
    yield '{} ::{} {}'.format(answer['web_url'], repo_name, summary)


def _load_config():
    config = {}
    for config_dir in BaseDirectory.load_config_paths('zebrahog'):
        config_path = os.path.join(config_dir, 'config.yaml')
        if os.path.exists(config_path):
            with open(config_path, encoding='utf-8') as f:
                part = parse_string(f.read())
            part.update(config)
            config = part
    assert config
    return config


config = _load_config()
credentials = config['credentials']


if __name__ == "__main__":
    import argparse

    async def _wrap(func, *args, **kwargs):
        async with gitlab() as gl:
            async for result in func(gl, *args, **kwargs):
                print(result)

    parser = argparse.ArgumentParser()
    parser.add_argument('--lazy', '-z', action='store_const', const=True,
                        help='be lazy and do not annoy GitLab much')
    subs = parser.add_subparsers(metavar='ACTION')
    subs.required = True

    pq_parser = subs.add_parser('patchlist', aliases=['pl'],
                                help="list pending patches (open merge requests)")
    pq_parser.add_argument('query', nargs='?', metavar='SEARCH QUERY',
                           help='query to search patch request (supports ::REPO format).')
    pq_parser.set_defaults(func=patch_list)

    pq_parser = subs.add_parser('patchqueue', aliases=['pq'],
                                help="list pending patches (open merge requests)")
    pq_parser.add_argument('--git-dir', metavar='GIT_DIR', default=None,
                           help='path to git repository with changes you want to merge')
    pq_parser.add_argument('--repo_name', metavar='REPO_NAME',
                           help='Exherbo repository name (default: profiles/repo_name)')
    pq_parser.add_argument('comment', nargs='*', metavar='COMMENT',
                           help='title for merge request (default: patch summary)')
    pq_parser.set_defaults(func=patch_queue)

    _args = parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(_wrap(**_args.__dict__))
    loop.close()
