from irc3.testing import BotTestCase


class TestCommands(BotTestCase):
    config = dict(includes=['irc3.plugins.command', 'zebrahog_plugin'])

    def test_help(self):
        bot = self.callFTU(nick='zebrahog')

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!help help')
        self.assertSent(['PRIVMSG bar :Show help', 'PRIVMSG bar :!help [<cmd>]'])

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!help')
        self.assertSent(['PRIVMSG bar :Available commands: !help, !patchlist, !patchqueue'])

    def test_patchlist(self):
        bot = self.callFTU(nick='zebrahog')

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!help patchlist')
        self.assertSent(['PRIVMSG bar :List pending patches (query by ::repo_name or keyword)',
                         'PRIVMSG bar :!patchlist [<query>]',
                         'PRIVMSG bar :Aliases: pl'])

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!help pl')
        self.assertSent(['PRIVMSG bar :List pending patches (query by ::repo_name or keyword)',
                         'PRIVMSG bar :!patchlist [<query>]',
                         'PRIVMSG bar :Aliases: pl'])

    def test_patchqueue(self):
        bot = self.callFTU(nick='zebrahog')

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!help patchqueue')
        self.assertSent(['PRIVMSG bar :!patchqueue <url> <repo> [<comment>...]'])

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!help pq')
        self.assertSent([
            'PRIVMSG bar :!patchqueue <url> <repo> [<comment>...]',

                         ])

        bot.dispatch(':bar!user@host PRIVMSG zebrahog :!patchqueue')
        self.assertSent(['PRIVMSG bar :Invalid arguments.'])
