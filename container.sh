#!/usr/bin/env bash

container=zebrahog-run

container_args=(
    --volume zebrahog-config:/etc/xdg
    --volume zebrahog-cache:/var/cache/zebrahog
    --mount type=tmpfs,destination=/tmp,tmpfs-size=2G
    --network=host
    --rm --read-only
    --name "$container"
)

hog() {
    docker run "${container_args[@]}" -it zebrahog "$@"
}

superhog() {
    docker run "${container_args[@]}" --user=root -it zebrahog "$@"
}

case "$1" in
run)
    set -ex
    exec docker run "${container_args[@]}" -d zebrahog
    ;;
build)
    set -ex
    exec docker build --network=host -t zebrahog . ;;
hog) shift; set -ex; hog "${@:-sh}" ;;
superhog) shift; set -ex; superhog "${@:-sh}" ;;
prime)
    set -ex
    xdg_home=/etc/xdg/zebrahog
    superhog sh -c 'chown -R zebrahog: "$XDG_CACHE_HOME/zebrahog" '"$xdg_home"

    hog git config --global user.email "virkony@gmail.com"
    hog git config --global user.name "zebrahog"

    key_file="$xdg_home/id_ed25519"
    if ! hog test -r "$key_file" ; then
        hog ssh-keygen -t ed25519 -C "zebrahog" -N "" -f "$key_file"
    fi
    hog cat "${key_file}.pub"
    echo "Ensure you added that SSH key to GitLab and run $0 prime-gitlab"
    ;;
prime-gitlab)
    set -ex
    hog git ls-remote git@gitlab.exherbo.org:exherbo/arbor.git HEAD
    ;;
*) cat "$0" ;;
esac
