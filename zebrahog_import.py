#!/usr/bin/python3
import asyncio

import aiohttp
import hashlib
import os.path
import re
from gidgetlab.abc import GitLabAPI
from tempfile import TemporaryDirectory
from typing import Optional

from zebrahog import gitlab, find_repo_root_projects, async_check_call, patch_queue

PATCH_SIZE_LIMIT = 10_000_000
VALID_PATCH_URI_RE = re.compile("|".join(
    [
        r'https?://paste\.pound-python\.org/raw/[0-9A-Za-z]/?',
    ]))


def normalize_patch_url(patch: str):
    assert VALID_PATCH_URI_RE.match(patch), "Unsupported patch URI %r" % (patch,)
    return patch


def _validate_patch_size(patch: str, size: int):
    assert size < PATCH_SIZE_LIMIT, "Patch %s exceeds limit %d (actual %d or more)" % (patch, PATCH_SIZE_LIMIT, size)


async def patch_import(gl: GitLabAPI, patch: str, repo_name: str, comment: Optional[str]=None):
    patch = normalize_patch_url(patch)
    if repo_name.startswith('::'):
        repo_name = repo_name[2:]
    projects = [project async for project in find_repo_root_projects(gl, repo_name)]
    assert len(projects) == 1, "Expecting only one project for repo %s, but found %s" % (repo_name, projects)
    [project] = projects
    with TemporaryDirectory(prefix='.zebrahog-') as workspace:
        await async_check_call('git', 'clone', project['ssh_url_to_repo'], workspace)
        git_dir = os.path.join(workspace, '.git')
        git_args = ['git',
                    '--work-tree=' + workspace,
                    '--git-dir=' + git_dir]

        proc = await asyncio.create_subprocess_exec(*git_args, 'am',
                                                    '--committer-date-is-author-date',  # stable commit hash
                                                    stdin=asyncio.subprocess.PIPE)
        patch_hash = hashlib.sha1()
        async with aiohttp.ClientSession() as session:
            async with session.get(patch) as response:
                normalize_patch_url(str(response.url))  # Just ensure we still in safe zone
                assert response.status == 200  # TODO: handle all 2xx
                print(patch, response.headers)
                _validate_patch_size(patch, int(response.headers.get('Content-Length', '0')))
                actual_size = 0
                while True:
                    chunk = await response.content.read(4096)
                    if not chunk:
                        break
                    actual_size += len(chunk)
                    _validate_patch_size(patch, actual_size)
                    patch_hash.update(chunk)
                    proc.stdin.write(chunk)
        proc.stdin.close()
        retcode = await proc.wait()
        if retcode != 0:
            raise ChildProcessError("Failed to apply %s in ::%s" % (patch, repo_name))
        branch = patch_hash.hexdigest()[:8]
        await async_check_call(*git_args, 'checkout', '-b', branch)
        footer_text = "Generated for %s" % (patch,)
        if comment:
            comment_words = (comment,)
        else:
            comment_words = tuple()
        async for message in patch_queue(gl, repo_name=repo_name, git_dir=git_dir, comment=comment_words,
                                         footer_text=footer_text, lazy=True):
            yield message

if __name__ == "__main__":
    import argparse

    async def _wrap(func, *args, **kwargs):
        async with gitlab() as gl:
            async for result in func(gl, *args, **kwargs):
                print(result)

    parser = argparse.ArgumentParser()
    parser.add_argument('patch', metavar='PATCH URI',
                        help='Link for fetching patch formed by git format-patch')
    parser.add_argument('repo_name', metavar='::REPO',
                        help='Exherbo repository to apply patch for (ex. ::x11)')
    parser.add_argument('comment', nargs='?', metavar='COMMENT',
                        help='Comment to override subject from patch file')
    _args = parser.parse_args()

    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    loop.run_until_complete(_wrap(patch_import, **_args.__dict__))
    loop.close()
