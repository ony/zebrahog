FROM alpine:latest

# Bring in git, Python 3.6+ and pip and prepare user
RUN apk update && apk upgrade && apk add python3 git openssh-client shadow
RUN useradd -d /etc/xdg/zebrahog -M -r zebrahog
RUN apk del shadow

WORKDIR /zebrahog
COPY . /zebrahog

# Install any needed packages specified in requirements.txt
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt

RUN echo -e "Host *\n  IdentityFile /etc/xdg/zebrahog/id_ed25519" | tee -a /etc/ssh/ssh_config

USER zebrahog

ENV XDG_CACHE_HOME /var/cache

VOLUME /etc/xdg
VOLUME /var/cache/zebrahog

CMD ["irc3", "-d", "-r", "config.ini"]
